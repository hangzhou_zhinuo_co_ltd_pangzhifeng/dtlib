# DTLib

#### 介绍
这是一个使用经典C++编写的微型模板库，包含了数组类，链表类，二叉树类等通用数据结构类型，非常适合于教学和科研场景。

#### 软件架构
![输入图片说明](DTLib.png)


#### Demo
```cpp
#include <iostream>
#include "DTLib/DynamicList.h"
#include "DTLib/LinkList.h"

using namespace DTLib;
using namespace std;

int main()
{
    cout << "ListDemo(): " << endl;

    LinkList<double> list;
    DynamicList<double> dl(50);

    for(int i=0; i<10; i++)
    {
        dl.insert(i/10.0);
    }

    dl.remove(5);
    
    for(int i=0; i<dl.length(); i++)
    {
        list.insert(dl[i]);
    }

    for(list.move(0); !list.end(); list.next())
    {
        cout << list.current() << endl;
    }

    return 0;
}
```